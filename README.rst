**qprof_interfaces**
====================

**qprof_interfaces** is a plugin for `qprof <https://gitlab.com/qcomputing/qprof/qprof>`_.
Its goal is to define the interface *qprof* modules should respect in order to
integrate a framework.

Installation
------------

From Gitlab
~~~~~~~~~~~

.. code:: shell

   git clone https://gitlab.com/cerfacs/qprof/qprof_interfaces.git
   pip install qprof_interfaces/

From PyPi
~~~~~~~~~

.. code:: shell

    pip install qprof_interface

Usage
-----

This plugin should not be needed by a simple user and is targeted at developers willing
to implement a plugin to support a new framework.
